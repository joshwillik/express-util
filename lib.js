let E = module.exports

E.handler = fn => {
  return async (req, res, next) => {
    try { await fn(req, res, next) }
    catch (e) { next(e) }
  }
}

E.rewrite = to => (req, res, next) => {
  req.url = to
  next()
}

E.to_handler = fn => (req, res, next) => {
  let params = Object.assign({}, req.query)
  fn(params).then(data => res.json(data)).catch(next)
}

E.truthy = value => {
  return ['1', 'true'].includes(value)
}
